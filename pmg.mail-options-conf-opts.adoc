`banner`: `<string>` ('default =' `ESMTP Proxmox`)::

ESMTP banner.

`before_queue_filtering`: `<boolean>` ('default =' `0`)::

Enable before queue filtering by pmg-smtp-filter

`conn_count_limit`: `<integer> (0 - N)` ('default =' `50`)::

How many simultaneous connections any client is allowed to make to this service. To disable this feature, specify a limit of 0.

`conn_rate_limit`: `<integer> (0 - N)` ('default =' `0`)::

The maximal number of connection attempts any client is allowed to make to this service per minute. To disable this feature, specify a limit of 0.

`dnsbl_sites`: `<string>` ::

Optional list of DNS white/blacklist domains (postfix option `postscreen_dnsbl_sites`).

`dnsbl_threshold`: `<integer> (0 - N)` ('default =' `1`)::

The inclusive lower bound for blocking a remote SMTP client, based on its combined DNSBL score (postfix option `postscreen_dnsbl_threshold`).

`dwarning`: `<integer> (0 - N)` ('default =' `4`)::

SMTP delay warning time (in hours). (postfix option `delay_warning_time`)

`filter-timeout`: `<integer> (2 - 86400)` ('default =' `600`)::

Timeout for the processing of one mail (in seconds)  (postfix option `smtpd_proxy_timeout` and `lmtp_data_done_timeout`)

`greylist`: `<boolean>` ('default =' `1`)::

Use Greylisting for IPv4.

`greylist6`: `<boolean>` ('default =' `0`)::

Use Greylisting for IPv6.

`greylistmask4`: `<integer> (0 - 32)` ('default =' `24`)::

Netmask to apply for greylisting IPv4 hosts

`greylistmask6`: `<integer> (0 - 128)` ('default =' `64`)::

Netmask to apply for greylisting IPv6 hosts

`helotests`: `<boolean>` ('default =' `0`)::

Use SMTP HELO tests. (postfix option `smtpd_helo_restrictions`)

`hide_received`: `<boolean>` ('default =' `0`)::

Hide received header in outgoing mails.

`maxsize`: `<integer> (1024 - N)` ('default =' `10485760`)::

Maximum email size. Larger mails are rejected. (postfix option `message_size_limit`)

`message_rate_limit`: `<integer> (0 - N)` ('default =' `0`)::

The maximal number of message delivery requests that any client is allowed to make to this service per minute.To disable this feature, specify a limit of 0.

`ndr_on_block`: `<boolean>` ('default =' `0`)::

Send out NDR when mail gets blocked

`rejectunknown`: `<boolean>` ('default =' `0`)::

Reject unknown clients. (postfix option `reject_unknown_client_hostname`)

`rejectunknownsender`: `<boolean>` ('default =' `0`)::

Reject unknown senders. (postfix option `reject_unknown_sender_domain`)

`smtputf8`: `<boolean>` ('default =' `1`)::

Enable SMTPUTF8 support in Postfix and detection for locally generated mail (postfix option `smtputf8_enable`)

`spf`: `<boolean>` ('default =' `1`)::

Use Sender Policy Framework.

`verifyreceivers`: `<450 | 550>` ::

Enable receiver verification. The value specifies the numerical reply code when the Postfix SMTP server rejects a recipient address. (postfix options `reject_unknown_recipient_domain`, `reject_unverified_recipient`, and `unverified_recipient_reject_code`)

